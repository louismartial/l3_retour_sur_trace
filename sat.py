def clause(chaine):
    clause = []
    i = 0
    while i < len(chaine):
        if chaine[i] != '\n':# Enlevable ?
            if (chaine[i] == '-' or chaine[i] >= '1' and chaine[i] <= '9') or (chaine[i] == '0' and chaine[i - 1] >= '1' and chaine[i - 1] <= '9'):
                #chiffre = ""
                # chiffre = chiffre[:] + chaine[i]
                chiffre = "" + chaine[i]
                j = i + 1
                while j < len(chaine) and chaine[j] != ' ':
                    # chiffre = chiffre[:] + chaine[j]
                    chiffre += chaine[j]
                    j = j + 1
                    i = i + 1
                chiffre = int(chiffre)
                clause.append(chiffre)
        i = i + 1
    return clause

def parseur(nom):
    F = []
    n = 0
    with open(nom) as f:
        for ligne in f:
            if ligne[0] != 'c':
                if ligne[0] == 'p':
                    l = ligne[:-2].split(' ')
                    n = int(l[2])
                elif len(ligne) > 2:
                    l = clause(ligne)
                    F.append(l)
    return (F, n)

def est_valide(F, A):
    for C in F:
        OK = False
        for l in C:
            if l * A[abs(l) - 1] > 0:
                OK = True
        if not OK:
            return False
    return True

def aff_suivante(A):
    n = len(A)
    i = 0
    while i < n and A[i] == 1:
        A[i] = -1
        i = i + 1
    if i == n:
        return None
    A[i] = 1
    return A

def sat_exhau(F, n):
    A = [-1 for i in range(n)]
    while not est_valide(F, A):
        A = aff_suivante(A)
        if not A:
            return None
    return A

"""
(F, n) = parseur('formules/random_13_unsat.cnf')
sat_exhau(F, n)
(F, n) = parseur('formules/random_17_unsat.cnf')
sat_exhau(F, n)

Sur ma machine personnelle, trouver une solution, pour une formule satisfiable prend environ une seconde, est reste possible jusqu'à 20 valeurs. Au delà, un temps de plus d'une seconde est requis.
Pour une forume insatisfiable c'est jusqu'à 17 valeurs.
"""

def elimination(F, n, b):
    P = []
    for clause in F:
        clauseB = []
        sat = False
        for litteral in clause:
            if abs(litteral) == n and litteral * b > 0:
                sat = True
            elif abs(litteral) != n:
                clauseB.append(litteral)
        if not sat:
            P.append(clauseB)
    return P

def sat_backtrack(F, n):
    if len(F) == 0:
        A = [1 for i in range(n)]# L'important est que A[n] = 1.
        return A
    for clause in F:
        if len(clause) == 0:
            return None
    for b in [1, -1]:
        P = elimination(F, n, b)
        A = sat_backtrack(P, n - 1)
        if A is not None:
            A.append(b)
            return A
    return None

"""
(F, n) = parseur('formules/simple_v3_c2.cnf')
sat_backtrack(F, n)
(F, n) = parseur('formules/quinn.cnf')
sat_backtrack(F, n)

(F, n) = parseur('formules/random_35_sat.cnf')
sat_backtrack(F, n)
(F, n) = parseur('formules/random_40_sat.cnf')
sat_backtrack(F, n)

(F, n) = parseur('formules/random_17_unsat.cnf')
sat_backtrack(F, n)
(F, n) = parseur('formules/random_25_unsat.cnf')
sat_backtrack(F, n)

Jusqu'à i = 40 pour trouver une solution en une seconde pour sat. Jusqu'à i = 25 pour unsat.

(F, n) = parseur('formules/hole_6.cnf')
sat_backtrack(F, n)

La formule hole_6.cnf n'est pas satisfiable.
"""
