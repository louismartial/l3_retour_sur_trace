# TP – Recherche exhaustive et retour sur trace

## SAT

**Objectifs de l'exercice.** L'objectif est d'écrire deux fonctions de résolution du problème SAT, l’une par recherche exhaustive et l’autre par backtrack. Des formules de test sont fournies dans des fichiers *.cnf*, et vous devrez écrire un parseur pour ce format.

**Rappel sur le problème.** Le problème SAT prend en entrée une formule sous forme normale conjonctive (formule CNF en abrégé), et renvoie une affectation des variables qui satisfait la formule, ou répond que
la formule n’est pas satisfiable. Une formule CNF est une conjonction de clauses, chaque clause étant une disjonction de littéraux, et un littéral étant soit une variable, soit sa négation. Par exemple, *ϕ(x<sub>1</sub> , x<sub>2</sub> , x<sub>3</sub> ) = (¬x<sub>1</sub> ∨ x<sub>2</sub> ) ∧ (x<sub>1</sub> ∨ x<sub>2</sub> ∨ ¬x<sub>3</sub>) ∧ ¬x<sub>2</sub>* est une formule CNF dont les trois clauses sont *(¬x<sub>1</sub> ∨ x<sub>2</sub>)*, *(x<sub>1</sub> ∨ x<sub>2</sub> ∨ ¬x<sub>3</sub>)* et *¬x<sub>2</sub>*.

**Représentation des formules.** On représente une formule CNF en Python par une liste de listes d’entiers. Une formule *ϕ* de *k* clauses est représentée par un liste de taille *k*. Chaque clause est elle-même représentée par un liste de littéraux. On représente le littéral *x<sub>i</sub>* par l’entier *i*, et le littéral *¬x<sub>i</sub>* par *−i*. Ainsi, la formule *ϕ* ci-dessus
est représentée par *F = [[-1,2],[1,2,-3],[-2]]*.

**Format de fichier.** Le format de fichier *DIMACS* pour représenter les formules est très proche de la représentation utilisée en Python. C’est un fichier texte, avec extension *.cnf*, constitué des listes suivantes :

- une ligne de la forme *p cnf 3 5* qui signifie que la formule possède *3* variables et *5* clauses ;
- pour chaque clause, une ligne de la forme *1 2 -3 0* pour la clause *x<sub>1</sub> ∨ x<sub>2</sub> ∨ ¬x<sub>3</sub>* , qui liste les littéraux de la clause et finit par un *0* ;
- des lignes de commentaires, n’importe où dans le fichier, commençant par le caractère *c* et qui peuvent (et doivent) être ignorées.

Par exemple, la formule de l’exemple peut être représentée par le fichier suivant :

> c Formule d'exemple, avec *3* variables et *3* clauses  
> p cnf 3 3  
> -1 2 0  
> 1 2 -3 0  
> -2 0

**Fichiers de test.** Les fichiers de tests sont contenus dans le dossier *formules*. La formule la plus simple est contenue dans le fichier *simple_c2.cnf*. Les fichiers *quinn.cnf* et *hole_6.cnf* sont des formules un peu plus compliquées. Enfin, les fichiers *random_i_sat.cnf* et *random_i_unsat.cnf* contiennent des formules générées aléatoirement, à *i* variables, satisfiables ou non satisfiables selon leur nom.

### Écrire un parseur pour les fichiers au format *DIMACS*

#### *clause(s)*

Fonction qui prend en entrée une chaîne de caractère *s* et renvoie la clause correspondante. On peut utiliser *s.split()* pour couper la chaîne au niveau des espaces et obtenir la liste des mots, et *int(x)* pour transformer une chaîne x en un entier. Tester avec la chaîne *"1 2 -3 0 \n"*.

#### *parseur(nom)*

Fonction qui prend en entrée un nom de fichier nom et renvoie le couple *(F, n)* où *F* est la formule représentée dans le fichier et *n* son nombre de variables. Parcourir toutes les lignes du fichier en ignorant les lignes de commentaire ; la ligne *p cnf …* permet
de connaître le nombre de variables, et les autres lignes de connaître les clauses. Tester avec le fichier *simple_c2.cnf* qui contient la formule *(x<sub>1</sub> ∨ ¬x<sub>3</sub>) ∧ (x<sub>2</sub> ∨ x<sub>3</sub> ∨ ¬x<sub>1</sub>)*.

### Implémenter la résolution de SAT par recherche exhaustive

On rappelle qu’on représente les booléens par des entiers *1* (pour *VRAI*) ou *−1* (pour *FAUX*) et qu’une affectation de *n* variables est une liste de taille *n*.

#### *est_valide(F, A)*

Fonction qui prend en entrée une formule *F* et une affectation *A* et teste si l’affectation *A* satisfait la formule *F*. Tester avec la formule contenue dans *simple_c2.cnf* et les affectations (*VRAI*, *VRAI*, *FAUX*) et (*FAUX*, *FAUX*, *FAUX*).

Exemple de test :

> F = parseur('formules/simple_c2.cnf')[0]  
> A1 = [1, 1, -1]  
> A2 = [-1, -1, -1]  
> est_valide(F, A1)  
> est_valide(F, A2)

#### *aff_suivante(A)*

Fonction qui prend en entrée une affectation *A* et calcule l’affectation suivante, avec l’algorithme vu en cours. Si *A* est la dernière affectation, l’algorithme renvoie *None*. Tester en affichant, dans l’ordre, toutes les affectations à *4* variables.

Exemple de test :

> A = [-1, -1, -1, -1]  
> for i in range(2**4 - 1): print(aff_suivante(A))

#### *sat_exhau(F, n)*

Fonction qui prend en entrée une formule *F* à *n* variables et renvoie une affectation *A* (de longueur *n*) qui satisfait *F*, s’il en existe une, et *None* sinon, par recherche exhaustive. Tester avec la formule du fichier *simple_c2.cnf*, puis avec celles des fichiers *random_…cnf* pour différentes valeurs de *i*. Jusqu’à quelle valeur de *i* pouvez-vous trouver une solution pour une formule satisfiable en environ une seconde ? Et pour les formules insatisfiables ?

### Implémenter la résolution de SAT par backtrack

#### *elimination(F, n, b)*

Fonction qui prend en entrée une formule *F* à *n* variables et un booléen *b* (entier ±1), et renvoie la formule à *n − 1* variables obtenue en éliminant la variable *x<sub>n</sub>* en lui affectant la valeur *b*. Tester que votre fonction est correcte sur de petits exemples.

Exemple de test:

> F = [[1, -3], [2, 3, -1]]  
> elimination(F, 3, 1)  
> elimination(F, 3, -1)

#### *sat_backtrack(F, n)*

Fonction qui prend en entrée une formule *F* à *n* variables et renvoie une affectation *A* (de longueur *n*) qui satisfait *F* s’il en existe une, *None* sinon, par backtrack. Tester avec la formule du fichier *simple_c2.cnf*, puis avec celles des fichiers *random_…cnf* pour différentes valeurs de *i*. Jusqu’à quelle valeur de *i* pouvez-vous trouver une solution pour une formule satisfiable en environ une seconde ? Et pour les formules insatisfiables ? La formule contenue dans le fichier *hole_6.cnf* est-elle satisfiable ?

## Sudoku

**Objectifs de l'exercice.** L'objectif est d’écrire un algorithme de résolution de grilles de Sudoku généralisées, c’est-à-dire des grilles de Sudoku *n<sup>2</sup> × n<sup>2</sup>* pour n’importe quel *n*.

**Rappel des règles.** Une grille complète de Sudoku *n<sup>2</sup> × n<sup>2</sup>* contient des entiers entre *1* et *n<sup>2</sup>* . Pour qu’elle soit valide, chaque ligne ne doit contenir que des entiers distincts, ainsi que chaque colonne et chaque zone (on découpe la grille *n<sup>2</sup> × n<sup>2</sup>* en *n<sup>2</sup>* zones de tailles *n × n*). L’entrée est une grille non complète (certaines cases sont vides). La sortie est la grille complétée ou *None* si la grille n’a aucune solution.

**Représentation informatique.** On représente la grille par une liste *G* de longueur *n<sup>4</sup>* . La case *G[u]* de *G* représente la case *(i, j)* de la grille, où *i* et *j* sont le quotient et le reste dans la division euclidienne de *u* par *n<sup>2</sup>* . Inversement, la case *(i, j)* de la grille est stockée dans la case *G[i<sub>n<sup>2</sup></sub> + j]* . Les cases contiennent des entiers entre *1* et *n<sup>2</sup>*, où *0* pour représenter une case vide.

**Format de fichier**. On représente une grille de Sudoku dans un fichier texte de la manière suivante. La première ligne du fichier contient l’entier *n* (*n* = *3* pour un Sudoku standard *9 × 9*). Les *n<sup>2</sup>* lignes suivantes du fichier contiennent chacun une ligne de la grille de Sudoku. Une ligne est représentée par la liste des entiers qu’elle contient, séparés par des espace, et peut contenir des *0* pour représenter les cases vides. Par exemple, la ligne *0 0 3 0 2 0 6 0 0* représente une ligne avec un *3* en *3<sup>e</sup>* colonne, un *2* en *5<sup>e</sup>* colonne et un *6* en *7<sup>e</sup>* colonne.

**Fichiers de test.** Le dossier *grilles* contient des grilles *9 × 9* (classées par difficulté).

### Lecture, écriture, affichage

#### *lecture_sudoku(nom)*

Fonction qui prend en entrée un nom de fichier, et renvoie le couple *(G,n)* où *G* est la grille de Sudoku de dimension *n<sup>2</sup> × n<sup>2</sup>* contenue dans le fichier.

#### *ecriture_sudoku(G, n, nom)*

Fonction qui prend en entrée une grille *G* de dimension *n<sup>2</sup> × n<sup>2</sup>* et un nom de fichier, et écrit la grille dans le fichier.

#### *affiche_sudoku(G, n)*

Fonction qui prend en entrée une grille *G* de dimension *n<sup>2</sup> × n<sup>2</sup>* et l’affiche à l’écran, ligne par ligne, avec des espaces entre les valeurs. Représente les cases vides par le caractère _.

### Résolution par backtrack

#### *zone(n, u)*

Fonction qui, étant donné *n* et un indice *u* (entre *0* et *n<sup>4</sup> − 1*), renvoie la liste des indices de toutes les cases qui sont dans la même zone que *u*. Par exemple, *zone(3,4)* doit renvoyer *[3, 4, 5, 12, 13, 14, 21, 22, 23]* et *zone(4, 50)* doit renvoyer *[0, 1, 2, 3, 16, 17, 18, 19, 32, 33, 34, 35, 48, 49, 50, 51]*.

#### *valide(G, n, u, x)*

Fonction qui prend en entrée une grille *G* de dimensions *n<sup>2</sup> × n<sup>2</sup>*, un indice *u < n<sup>4</sup>* et une valeur *x*, et renvoie *True* si ajouter la valeur *x* en case d’indice *u* ne crée aucun conflit, et *False* sinon. Tester sur quelques exemples.

#### *sudoku(G, n, u = 0)*

Fonction qui prend en entrée une grille *G* de dimensions *n<sup>2</sup> × n<sup>2</sup>* et un indice (optionnel) *u < n4* , et complète la grille *G* et renvoie *True* si la grille possède une solution, et ne modifie pas la grille et renvoie *False* sinon, en utilisant un algorithme de type backtrack. Tester sur différentes grilles. Jusqu’à quelle dimension de grille pouvez-vous résoudre le problème en environ une seconde ?

Exemple de test :

> G, n = lecture_sudoku("sudoku_3_moyen_1.txt")  
> sudoku(G, n)