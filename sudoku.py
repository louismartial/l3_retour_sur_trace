def lecture_sudoku(nom):
    G = []
    n = 0
    with open(nom) as fichier:
        premiereLigne = True
        for ligne in fichier:
            if premiereLigne:
                n = int(ligne[0])
                premiereLigne = False
            else:
                L = ligne.split(' ')
                del L[-1]
                L = [int(i) for i in L]
                G = G + L
        return (G, n)

def ecriture_sudoku(G, n, nom):
    with open(nom, 'w') as fichier:
        fichier.write(str(n) + '\n')
        k = 0
        for i in range(n * n):
            for j in range(n * n):
                fichier.write(str(G[k]))
                fichier.write(' ')
                k = k + 1
            fichier.write('\n')

def afficher_sudoku(G, n):
    k = 0
    for i in range(n * n):
        for j in range(n * n):
            if G[k] == 0:
                print('_', end =' ')
            else:
                print(str(G[k]), end = ' ')
            k = k + 1
        print('')

def zone(n, u):
    Z = []
    i = u // n**2
    j = u % n**2
    for k in range(n * n):
        for l in range(n * n):
            if (i // n == k // n) and (j // n == l // n):
                Z.append(k * n**2 + l)
    return Z

def valide(G, n, u, x):
    i = u // n**2
    j = u % n**2
    for k in range(n**2):
        if k != i and G[k * n**2 + j] == x:
            return False
        if k != j and G[i * n**2 + k] == x:
            return False
    for indice in zone(n, u):
        if indice != u:
            if G[indice] == x:
                return False
    return True

def sudoku(G, n, u = 0):
    while u < n**4 and G[u] != 0:
        u = u + 1
    if u == n**4:
        ecriture_sudoku(G, n, 'sortie.txt')
        return True
    for x in range(1, n**2 + 1):
        if valide(G, n, u, x):
            G[u] = x
            if sudoku(G, n, u + 1):
                return True
    G[u] = 0
    return False

# Sur ma machine, une solution au problème est trouvable en environ une seconde, pour une grille de dimension 9.
